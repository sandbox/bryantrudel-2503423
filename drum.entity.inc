<?php
/**
 * @file
 * drum.entity.inc
 */

/**
 * Imeplements hook_entity_info().
 */
function drum_entity_info() {
  $info = array();

  $info['drum'] = array(
    'label' => t('DRUM'),
    'base table' => 'drum_entity',
    'entity keys' => array(
      'id' => 'did',
    ),
    'module' => 'drum',
    'entity class' => 'Entity',
    'controller class' => 'DrumEntityController',
  );

  $info['drum_status'] = array(
    'label' => t('DRUM status'),
    'base table' => 'drum_status',
    'entity keys' => array(
      'id' => 'sid',
    ),
    'module' => 'drum',
    'entity class' => 'Entity',
    'controller class' => 'DrumStatusEntityController',
  );

  return $info;

}
