<?php

namespace Drum\Service;


class PingService {

  public function ping($host, $port = 80, $timeout = 6) {

    $errno = 0;
    $errstr = 'error';
    $socket = @fsockopen($host, $port, $errno, $errstr, $timeout);

    return $socket;
  }

  public function getHeaders($url) {

    return get_headers($url);

  }

  public function checkStatus($url) {
    $agent = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; pt-pt) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27";

    // initializes curl session
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);

    // execute
    $then = $this->milliseconds();
    curl_exec($ch);
    $now = $this->milliseconds();
    $difference = $now - $then;
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


    $status = array(
      'status' => $httpcode ,
      'response_time' => $difference,
    );
    curl_close($ch);
    return $status;
  }

  public function milliseconds() {
    $mt = explode(' ', microtime());
    return $mt[1] * 1000 + round($mt[0] * 1000);
  }

}
