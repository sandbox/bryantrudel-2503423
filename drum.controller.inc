<?php
/**
 * @file
 * drum.controller.inc
 */

/**
 * Extending the EntityAPIController for the Project entity.
 */
class DrumEntityController extends EntityAPIController {

  /**
   * Extending buildContent from EntityAPI.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    // Our additions to the $build render array.
    $build['hostname'] = array(
      '#type' => 'markup',
      '#markup' => check_plain($entity->hostname),
      '#prefix' => '<div class="drum-hostname">',
      '#suffix' => '</div>',
    );

    return $build;

  }


}

/**
 * Extending the EntityAPIController for the Project entity.
 */
class DrumStatusEntityController extends EntityAPIController {

  /**
   * Extending buildContent from EntityAPI.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    // Our additions to the $build render array.
    $build['hostname'] = array(
      '#type' => 'markup',
      '#markup' => check_plain($entity->sid),
      '#prefix' => '<div class="drum-sid">',
      '#suffix' => '</div>',
    );

    return $build;

  }


}
